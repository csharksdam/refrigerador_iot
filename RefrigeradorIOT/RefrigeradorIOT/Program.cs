﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Diagnostics;

namespace RefrigeradorIOT
{
    class Program
    {
        private static Socket socket = null;
        private static bool corriendo = false;
        private static IPEndPoint puntoLocal = null;

        private static void Escuchador()
        {
            //instanciamos el socket 
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

            socket.Bind(puntoLocal);
            Console.WriteLine("escuchando...");

            byte[] buffer = new byte[1024];
            //definir objeto para obtener la IP y Puerto de quien nos envía los datos 
            EndPoint ipRemota = new IPEndPoint(IPAddress.Any, 11000); 
                                                                     
            corriendo = true;
            //ciclo que permitirá escuchar continuamente mientras se esté corriendo el servidor 
            while (corriendo)
            {
                if (socket.Available == 0)  
                {
                    Thread.Sleep(200); 
                    continue;  
                }
                //en caso de que si hayan datos disponibles debemos leerlos 
                //indicamos el buffer donde se guardarán los datos y enviamos ipRemota como parámetro de referencia 
                //adicionalmente el método ReceiveFrom nos devuelve cuandos bytes se leyeron 
                int contadorLeido = socket.ReceiveFrom(buffer, ref ipRemota);
                //ahora tenemos los datos en buffer (1024 bytes) pero sabemos cuantos recibimos (contadorLeido) 
                //convertimos esos bytes a string 
                string datosRecibidos = Encoding.Default.GetString(buffer, 0, contadorLeido);
                Console.WriteLine("Recibí: " + datosRecibidos);
                Enviar(datosRecibidos);
            }
        }

        public static void Enviar(string horas)
        {
            UdpClient udpClient = new UdpClient(11100);
            udpClient.Connect("172.17.20.232", 9000); // mirar detenidamente
            string mensaje = getTemperatures(horas);

            string datosAEnviar = mensaje;



            byte[] datosEnBytes = Encoding.Default.GetBytes(datosAEnviar);
            udpClient.Send(datosEnBytes, datosEnBytes.Length);
        }

        public static string getTemperatures(string horas_men)
        {
            /*------------------------ GET TEMPERATURES -----------------------------*/

            //Pillar la informacion del archivo


            string[] horas_m = horas_men.Split('|');
            int horaent = Int32.Parse(horas_m[0]);
            int horasal = Int32.Parse(horas_m[1]);


            string[] lines = System.IO.File.ReadAllLines(@".\datos\IOT.txt");
            string temperaturas = "";
            int maxtemp = 0;
            int mintemp = 9999;

            //Pillar la informacion
            foreach (string line in lines)
            {

                

                string[] valores = line.Split('|');

                int horas = Int32.Parse(valores[1]);
                int tempsmin = Int32.Parse(valores[2]);
                int tempsmax = Int32.Parse(valores[3]);


                if ((horas >= horaent) && (horas <= horasal))
                {
                    if (tempsmax > maxtemp)
                    {
                        maxtemp = tempsmax;
                    }

                    if (tempsmin < mintemp)
                    {
                        mintemp = tempsmin;

                    }
                    
                }

                
            }//End ForEach    
            temperaturas = maxtemp + "|" + mintemp;
            return temperaturas;

        }



        static void Main(string[] args)
        {
            /*------------------------ SERVER -----------------------------*/

            

            IPAddress ipEscucha = IPAddress.Any; //indicamos que escuche por cualquier tarjeta de red local 
                                                 //IPAddress ipEscucha = IPAddress.Parse("0.0.0.0"); //o podemos indicarle la IP de la tarjeta de red local 
            int puertoEscucha = 10000; //puerto por el cual escucharemos datos             
            puntoLocal = new IPEndPoint(ipEscucha, puertoEscucha); //definimos la instancia del IPEndPoint 
                                                                   //lanzamos el escuchador por medio de un hilo 
            new Thread(Escuchador).Start();
            Console.ReadLine(); //esperar a que el usuario escriba algo y de enter 
            corriendo = false; //finalizar el servidor 


        }
    }

}
    

